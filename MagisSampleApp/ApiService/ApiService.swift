//
//  ApiService.swift
//  MagisSampleApp
//
//  Created by Lance Hirsch on 2/16/19.
//  Copyright © 2019 Lance Hirsch. All rights reserved.
//

import UIKit

class ApiService: NSObject {

    static let sharedInstance = ApiService()
    
    func fetchProducts(completion: @escaping ([Product]) -> ()) {
        let urlString = "https://magis-technology.herokuapp.com/products"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            DispatchQueue.main.async {
                if let error = error {
                    print("No data returned: ", error)
                    return
                }
                
                guard let data = data else { return }
                
                var products = [Product]()
                
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        if let children = json["products"] as? [Any] {
                            for child in children {
                                if let dictionary = child as? JSONDictionary {
                                    if let product = Product(dictionary: dictionary) {
                                        products.append(product)
                                    }
                                }
                            }
                        }
                    }
                } catch let error {
                    print("Error parsing JSON: ", error)
                }
                completion(products)
            }
            }.resume()
    }

}
