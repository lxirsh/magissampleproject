//
//  SelectedItem.swift
//  MagisSampleApp
//
//  Created by Lance Hirsch on 2/17/19.
//  Copyright © 2019 Lance Hirsch. All rights reserved.
//

import UIKit

struct SelectedItem {
    var product: Product
    var price: Price
    var amount: Int
    
    init(product: Product, price: Price) {
        self.product = product
        self.price = price
        amount = 0
    }
}
