//
//  Product.swift
//  MagisSampleApp
//
//  Created by Lance Hirsch on 2/16/19.
//  Copyright © 2019 Lance Hirsch. All rights reserved.
//

import Foundation

typealias JSONDictionary = Dictionary<String, Any>

struct Product {
    var id: String
    var available: Int
    var isActive: Bool
    var picture: String
    var prices: [Price]
    var title: String
    
    init?(dictionary: JSONDictionary) {
        guard let id = dictionary["_id"] as? String,
            let available = dictionary["available"] as? Int,
            let isActive = dictionary["isActive"] as? Bool,
            let picture = dictionary["picture"] as? String,
            let jsonArray = dictionary["prices"] as? [Any],
            let prices = Price.getPrices(from: jsonArray),
            let title = dictionary["title"] as? String
        else { return nil }
        
        self.id = id
        self.available = available
        self.isActive = isActive
        self.picture = picture
        self.prices = prices
        self.title = title
    }
    
    
}

struct Price {
    var currency: String
    var price: Int
    var unit: String
    
    init?(dictionary: JSONDictionary) {
        
        guard let
            currency = dictionary["currency"] as? String,
            let price = dictionary["price"] as? Int,
            let unit = dictionary["unit"] as? String else { return nil }
        
        self.currency = currency
        self.price = price
        self.unit = unit
    }
}

extension Price {
    static func getPrices(from results: [Any]) -> [Price]? {
        var prices = [Price]()
        
        for result in results {
            if let result = result as? JSONDictionary {
                let price = Price(dictionary: result)
                
                if let price = price {
                    prices.append(price)
                }
            }
        }
        
        return prices
    }
}


