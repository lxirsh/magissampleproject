//
//  ProductSelectionManager.swift
//  MagisSampleApp
//
//  Created by Lance Hirsch on 2/17/19.
//  Copyright © 2019 Lance Hirsch. All rights reserved.
//

import UIKit

class ProductSelectionManager {
    
    static let sharedInstance = ProductSelectionManager()
    
    var amount: Int = 0
    
    var previousSelectedCell: UITableViewCell? {
        didSet {
            // Change the image of the previously selected cell to show it unchecked
            if let cell = previousSelectedCell as? PriceCell {
                let deselectedImage = UIImage(named: "UnChecked")
                cell.selectionButton.setImage(deselectedImage, for: .normal)
            }
        }
    }
    
    var currentSelectedCell: UITableViewCell? {
        didSet {
            previousSelectedCell = oldValue
        }
    }
    
    var selectedItem: SelectedItem? {
        didSet {
            print("new item in basket")
        }
    }
}
