//
//  ConfirmationView.swift
//  MagisSampleApp
//
//  Created by Lance Hirsch on 2/17/19.
//  Copyright © 2019 Lance Hirsch. All rights reserved.
//

import UIKit

class ConfirmationView: UIView {

    override init(frame: CGRect) {

        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpProductImage(imageUrl: String) {
        productImageView.loadImage(from: imageUrl)
    }

    
    let contentView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("X", for: .normal)
        button.backgroundColor = UIColor.lightGray
        button.layer.cornerRadius = 4
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let productImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.red
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.text = "Toplam Fiyat"
        label.textAlignment = .center
        label.backgroundColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let totalPriceLabel: UILabel = {
        let label = UILabel()
        label.text = "25 TL"
        label.textAlignment = .center
        label.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let productCountLabel: UILabel = {
        let label = UILabel()
        label.text = "5 kg     Urun 1"
        label.textAlignment = .center
        label.backgroundColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    
    @objc func dismissView() {
        removeFromSuperview()
    }
    
    func setupView() {
        backgroundColor = UIColor.clear
        
        // Setup labels
        let productManager = ProductSelectionManager.sharedInstance
        if let selectedItem = productManager.selectedItem {
            productImageView.loadImage(from: selectedItem.product.picture)
            productCountLabel.text = String(productManager.amount) + " " + selectedItem.price.unit + "\t" + selectedItem.product.title
            totalPriceLabel.text = String(productManager.amount * selectedItem.price.price) + " " + selectedItem.price.currency
        }
        
        addSubview(contentView)
        contentView.addSubview(closeButton)
        contentView.addSubview(productImageView)
        contentView.addSubview(priceLabel)
        contentView.addSubview(totalPriceLabel)
        contentView.addSubview(productCountLabel)
        
        NSLayoutConstraint.activate([
            contentView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            contentView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            contentView.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8),
            contentView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8)
            ])
        
        NSLayoutConstraint.activate([
            closeButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            closeButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -8),
            closeButton.heightAnchor.constraint(equalToConstant: 25),
            closeButton.widthAnchor.constraint(equalToConstant: 25)
            ])
        
        NSLayoutConstraint.activate([
            productImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 40),
            productImageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            productImageView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5),
            productImageView.widthAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5)
            ])
        
        NSLayoutConstraint.activate([
            priceLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8),
            priceLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            priceLabel.rightAnchor.constraint(equalTo: contentView.centerXAnchor),
            priceLabel.heightAnchor.constraint(equalToConstant: 30)
            ])
        
        NSLayoutConstraint.activate([
            totalPriceLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -8),
            totalPriceLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            totalPriceLabel.leftAnchor.constraint(equalTo: contentView.centerXAnchor),
            totalPriceLabel.heightAnchor.constraint(equalToConstant: 28)
            ])
        
        // Setup seperator line between the labels
        let firstSeperatorLine = UIView()
        firstSeperatorLine.backgroundColor = UIColor.lightGray
        firstSeperatorLine.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(firstSeperatorLine)
        
        let topSeperatorLine = UIView()
        topSeperatorLine.backgroundColor = UIColor.lightGray
        topSeperatorLine.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(topSeperatorLine)

        
        NSLayoutConstraint.activate([
            firstSeperatorLine.rightAnchor.constraint(equalTo: totalPriceLabel.rightAnchor),
            firstSeperatorLine.bottomAnchor.constraint(equalTo: priceLabel.topAnchor),
            firstSeperatorLine.leftAnchor.constraint(equalTo: priceLabel.leftAnchor),
            firstSeperatorLine.heightAnchor.constraint(equalToConstant: 1)
            ])
        
        NSLayoutConstraint.activate([
            productCountLabel.rightAnchor.constraint(equalTo: totalPriceLabel.rightAnchor),
            productCountLabel.bottomAnchor.constraint(equalTo: firstSeperatorLine.topAnchor),
            productCountLabel.leftAnchor.constraint(equalTo: priceLabel.leftAnchor),
            productCountLabel.heightAnchor.constraint(equalToConstant: 30)
            ])
        
        NSLayoutConstraint.activate([
            topSeperatorLine.rightAnchor.constraint(equalTo: totalPriceLabel.rightAnchor),
            topSeperatorLine.bottomAnchor.constraint(equalTo: productCountLabel.topAnchor),
            topSeperatorLine.leftAnchor.constraint(equalTo: priceLabel.leftAnchor),
            topSeperatorLine.heightAnchor.constraint(equalToConstant: 1)
            ])

    }
}
