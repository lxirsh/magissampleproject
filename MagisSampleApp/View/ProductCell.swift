//
//  ProductCell.swift
//  MagisSampleApp
//
//  Created by Lance Hirsch on 2/16/19.
//  Copyright © 2019 Lance Hirsch. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    let cellId = "cellId"
    
    var prices = [Price]()
        
    var product: Product? {
        didSet {
            if let product = product {
                productNameLabel.text = product.title
                productCountLabel.text = "Stok Sayısı: " + String(product.available)
                setUpProductImage()
                
                for price in product.prices {
                    prices.append(price)
                }
            }
        }
    }
    
    // Used to keep track of which CollectionViewCell a price is selected from
    var productCellIndexPath: IndexPath?
    
    var previouslySelecteItem: SelectedItem?
    var selectedItem: SelectedItem?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setUpProductImage() {
        if let product = product {
            productImageView.loadImage(from: product.picture)
        }
    }
    
    let productImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let productNameLabel: UILabel = {
        let label = UILabel()
        label.text = "urun #"
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let productCountLabel: UILabel = {
        let label = UILabel()
        label.text = "Stok Sayısı: 100"
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let pricesTableView: UITableView = {
        let tableview = UITableView()
        tableview.backgroundColor = UIColor.lightGray
        tableview.translatesAutoresizingMaskIntoConstraints = false
        return tableview
    }()
    
    func setupViews() {
        backgroundColor = UIColor.lightGray
        addSubview(productImageView)
        addSubview(productNameLabel)
        addSubview(productCountLabel)
        addSubview(pricesTableView)
        
        pricesTableView.delegate = self
        pricesTableView.dataSource = self
        pricesTableView.register(PriceCell.self, forCellReuseIdentifier: cellId)
        pricesTableView.separatorInset = UIEdgeInsets.zero

        // x,y,h,w
        NSLayoutConstraint.activate([
            productImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            productImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            productImageView.heightAnchor.constraint(equalToConstant: 64),
            productImageView.widthAnchor.constraint(equalToConstant: 64)
            ])
        
        NSLayoutConstraint.activate([
            productNameLabel.leftAnchor.constraint(equalTo: productImageView.rightAnchor, constant: 8),
            productNameLabel.topAnchor.constraint(equalTo: productImageView.topAnchor, constant: 4),
            productNameLabel.widthAnchor.constraint(equalToConstant: 110),
            productNameLabel.heightAnchor.constraint(equalToConstant: 20)
            ])
        
        NSLayoutConstraint.activate([
            productCountLabel.leftAnchor.constraint(equalTo: productImageView.rightAnchor, constant: 8),
            productCountLabel.topAnchor.constraint(equalTo: productNameLabel.bottomAnchor, constant: 8),
            productCountLabel.widthAnchor.constraint(equalToConstant: 110),
            productCountLabel.heightAnchor.constraint(equalToConstant: 20)
            ])
        
        NSLayoutConstraint.activate([
            pricesTableView.leftAnchor.constraint(equalTo: productNameLabel.rightAnchor, constant: 8),
            pricesTableView.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
            pricesTableView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8),
            pricesTableView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0)
            ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ProductCell: UITableViewDataSource, UITableViewDelegate, PriceCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PriceCell
        let price = prices[indexPath.item]
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.lightGray
        cell.delegate = self
        cell.price = price
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25
    }
    
    // PriceCellDelegate
    func didSelectPrice(cell: UITableViewCell) {
        print("new price selected")
        let productManager = ProductSelectionManager.sharedInstance
        productManager.currentSelectedCell = cell
        
        guard let selectedRow = pricesTableView.indexPath(for: cell),
            let product = product else { return }
        let price = prices[selectedRow.item]
        let newItem = SelectedItem(product: product, price: price)
        productManager.selectedItem = newItem
    }
    
}

