//
//  PriceCell.swift
//  MagisSampleApp
//
//  Created by Lance Hirsch on 2/17/19.
//  Copyright © 2019 Lance Hirsch. All rights reserved.
//

import UIKit

class PriceCell: UITableViewCell {

    weak var delegate: PriceCellDelegate?

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var price: Price? {
        didSet {
            if let price = price {
                priceLabel.text = "1 " + price.unit + " -> " + String(price.price) + " " + price.currency
            }
        }
    }
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let selectionButton: UIButton = {
        let button = UIButton()
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setupViews()
        let buttonImage = UIImage(named: "UnChecked")
        selectionButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        selectionButton.setImage(buttonImage, for: .normal)
        selectionButton.tintColor = .black
        selectionButton.addTarget(self, action: #selector(didSelectRow), for: .touchUpInside)
    
        accessoryView = selectionButton
    }
    
    @objc func didSelectRow() {
        delegate?.didSelectPrice(cell: self)
        changeSelectionButtonImageToChecked()
    }
    
    func changeSelectionButtonImageToChecked() {
        let buttonImage = UIImage(named: "Checked")
        selectionButton.setImage(buttonImage, for: .normal)
    }
    
    func setupViews() {
        addSubview(priceLabel)
        
        // x,y,h,w
        NSLayoutConstraint.activate([
            priceLabel.leftAnchor.constraint(equalTo: self.leftAnchor),
            priceLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            priceLabel.heightAnchor.constraint(equalTo: self.heightAnchor),
            priceLabel.widthAnchor.constraint(equalTo: self.widthAnchor)
            ])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

protocol PriceCellDelegate: class {
    
    func didSelectPrice(cell: UITableViewCell)
}
