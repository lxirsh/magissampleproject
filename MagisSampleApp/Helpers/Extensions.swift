//
//  Extensions.swift
//  MagisSampleApp
//
//  Created by Lance Hirsch on 2/17/19.
//  Copyright © 2019 Lance Hirsch. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    
    func loadImage(from urlString: String) {
        
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
        
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            
            if let error = error {
                print(error)
                return
            }
            
            guard let data = data else { return }
            
            DispatchQueue.main.async {
                
                guard let imageToCache = UIImage(data: data) else { return }
                imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                
                self.image = imageToCache
            }
        }.resume()
    }
}

extension UIColor {
    
    static let gainsboro = UIColor(red: 220, green: 220, blue: 220, alpha: 1)
}
