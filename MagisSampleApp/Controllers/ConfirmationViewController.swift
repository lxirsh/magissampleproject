//
//  ConfirmationViewController.swift
//  MagisSampleApp
//
//  Created by Lance Hirsch on 2/17/19.
//  Copyright © 2019 Lance Hirsch. All rights reserved.
//

import UIKit

class ConfirmationViewController: UIViewController {
    
    var confirmationView: ConfirmationView {
        return self.view as! ConfirmationView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func loadView() {
        self.view = ConfirmationView(frame: UIScreen.main.bounds)
    }



}
