//
//  ProductViewController.swift
//  MagisSampleApp
//
//  Created by Lance Hirsch on 2/16/19.
//  Copyright © 2019 Lance Hirsch. All rights reserved.
//

import UIKit

class ProductViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var products = [Product]()
    
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = UIColor.white
        collectionView.register(ProductCell.self, forCellWithReuseIdentifier: cellId)

        // Tap gesture to dismiss the number pad
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProductViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

        setUpPurchaseComponents()
        fetchProducts()
        setUpKeyboardObservers()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func setUpKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    @objc func handleKeyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let keyboardDuration: TimeInterval = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            containerViewBottomAnchor?.constant = -keyboardHeight
            UIView.animate(withDuration: keyboardDuration) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func handleKeyboardWillHide(notification: NSNotification) {
        
        if let keyboardDuration: TimeInterval = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval {
            
            containerViewBottomAnchor?.constant = 0
            UIView.animate(withDuration: keyboardDuration) {
                self.view.layoutIfNeeded()
            }
        }
    }

    
    let numberEntryTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = UIColor.white
        textField.keyboardType = .decimalPad
        return textField
    }()
    
    let buyButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("SATIN AL", for: .normal)
        button.backgroundColor = UIColor.orange
        button.layer.cornerRadius = 4
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(buyButtonSelected(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    @objc func buyButtonSelected(_ sender: UIButton) {
        
        if let validAmount = validateIntegerInput() {
            let productManager = ProductSelectionManager.sharedInstance
            if productManager.selectedItem != nil {
                productManager.amount = validAmount
                dismissKeyboard()
                let confirmationViewController = ConfirmationViewController()
                self.addChild(confirmationViewController)
                confirmationViewController.view.frame = self.view.frame
                self.view.addSubview(confirmationViewController.view)
                confirmationViewController.didMove(toParent: self)
            } else {
                let alertMessage = "Lütfen bir ürün seçiniz."
                alertUser(title: nil, message: alertMessage)
            }
            
        } else {
            let alertMessage = "Lütfen sadece rakam ile giriş yapanız."
            alertUser(title: nil, message: alertMessage)
        }
    }
    
    func validateIntegerInput() -> Int? {
        if let text = numberEntryTextField.text,
            let validInput = Int(text) {
            return validInput
        }
        return nil
    }
    
    func alertUser(title: String?, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "TAMAM", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func fetchProducts() {
        ApiService.sharedInstance.fetchProducts { (products) in
            self.products = products
            self.collectionView.reloadData()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ProductCell
        let product = products[indexPath.item]
        cell.product = product
        cell.productCellIndexPath = indexPath
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: 90)
        
    }
    
    var containerViewBottomAnchor: NSLayoutConstraint?
        
    func setUpPurchaseComponents() {
        let containerView = UIView()
        containerView.backgroundColor = UIColor.lightGray
        containerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(containerView)
        containerView.addSubview(buyButton)
        containerView.addSubview(numberEntryTextField)
        
        containerViewBottomAnchor = containerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        containerViewBottomAnchor?.isActive = true
        
        if #available(iOS 11.0, *) {
            
            NSLayoutConstraint.activate([
                containerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                containerView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor),
                containerView.heightAnchor.constraint(equalToConstant: 40)
                ])
        } else {
            NSLayoutConstraint.activate([
                containerView.leftAnchor.constraint(equalTo: view.leftAnchor),
                containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                containerView.widthAnchor.constraint(equalTo: view.widthAnchor),
                containerView.heightAnchor.constraint(equalToConstant: 40)
                ])
        }
    
        NSLayoutConstraint.activate([
            buyButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            buyButton.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            buyButton.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.8),
            buyButton.widthAnchor.constraint(equalToConstant: 100)
            ])
        
        NSLayoutConstraint.activate([
            numberEntryTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            numberEntryTextField.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            numberEntryTextField.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.8),
            numberEntryTextField.trailingAnchor.constraint(equalTo: buyButton.leadingAnchor, constant: -8)
            ])
        
        
    }

}
